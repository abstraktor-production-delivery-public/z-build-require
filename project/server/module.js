
'use strict';

const Path = require('path');
let Module = require('module');
const originalRequire = Module.prototype.require;


class AbsModule {
  static calculateDynamicConfig() {
    let nextPropertyName = '';
    let nextProperyValueMulti = '';
    const parameters = new Map();
    const tasks = [];
    const argv = process.argv.slice(2);
    argv.forEach((arg) => {
      if(0 === nextPropertyName.length) {
        if(arg.startsWith('--')) {
          nextPropertyName = arg.substring(2);
        }
        else {
          tasks.push(arg);
        }
      }
      else if(0 !== nextProperyValueMulti.length) {
        if(arg.endsWith('"')) {
          nextProperyValueMulti += arg.substring(0, arg.length - 1);
          tasks.push(nextProperyValueMulti);
          nextProperyValueMulti = '';
        }
        else {
          nextProperyValueMulti += arg;
        }
      }
      else {
        if(arg.startsWith('"')) {
          nextProperyValueMulti = arg.substring(1);
        }
        else if(arg.startsWith('--')) {
          parameters.set(nextPropertyName, undefined);
          nextPropertyName = arg.substring(2);
        }
        else {
          parameters.set(nextPropertyName, arg);
          nextPropertyName = '';
        }
      }
    });
    if(0 !== nextPropertyName.length) {
      parameters.set(nextPropertyName, undefined);
    }
    return {
      app: '',
      parameters: parameters,
      tasks: tasks
    };
  }
  
  static setPaths(nodePathAbstractor, paths) {
    const delimiter = 'win32' === process.platform ? ';' : ':';
    const extraPaths = paths ? delimiter + paths.join(delimiter) : '';
    if(nodePathAbstractor.nodePath) {
      process.env.NODE_PATH = `${nodePathAbstractor.nodePath}${delimiter}${nodePathAbstractor.pathLayers}${extraPaths}`;
    }
    else {
      process.env.NODE_PATH = `${nodePathAbstractor.pathLayers}${extraPaths}`;
    }
    require('module').Module._initPaths();
  }
  
  static abstractorRegisterApiAndStacks(paths) {
    const nodePathAbstractor = Reflect.get(global, 'node_path@abstractor');
    AbsModule.setPaths(nodePathAbstractor, paths);
    nodePathAbstractor.isApiAndStacksRegistered = true;
  }
  
  static abstractorIsApiAndStacksRegistered() {
    const nodePathAbstractor = Reflect.get(global, 'node_path@abstractor');
    return nodePathAbstractor.isApiAndStacksRegistered;
  }
  
  static init(dev, organization, dynamicConfig) {
    const pathLayers = Path.resolve('./dist/Layers/');
    const nodePathAbstractor = {
      nodePath: process.env.NODE_PATH,
      pathLayers: pathLayers,
      isApiAndStacksRegistered: false
    };
    Reflect.set(global, 'node_path@abstractor', nodePathAbstractor);
    AbsModule.setPaths(nodePathAbstractor, null);
    if(!dynamicConfig) {
      dynamicConfig = AbsModule.calculateDynamicConfig();
    }
    Reflect.set(global, 'dynamicConfig@abstractor', dynamicConfig);
    const parameters = [];
    dynamicConfig.parameters.forEach((value, key) => {
      parameters.push([key, value]);
    });
    const dynamicConfigStringified = JSON.stringify({
      parameters: parameters,
      tasks: dynamicConfig.tasks
    }).replace(/,/g, ';');
    Reflect.set(global, 'dynamicConfigStringified@abstractor', dynamicConfigStringified);
    if(dev) {
      dynamicConfig.parameters.set('dev', true);
      const pathProject = Path.resolve('..');
      Module.prototype.require = function() {
        const argument1 = arguments[0];
        if(argument1.startsWith('z-')) {
          if(argument1.startsWith('z-b')) {
            arguments[0] = `${pathProject}/${argument1}`;
          }
          else {
            arguments[0] = `${pathLayers}/${argument1}`;
          }
        }
        return originalRequire.apply(this, arguments);
      };
    }
    else if(organization) {
      const pathOrganization = `${organization}/`;
      Module.prototype.require = function() {
        const argument1 = arguments[0];
        if(argument1.startsWith('z-')) {
          if(!argument1.startsWith('z-b')) {
            arguments[0] = `${pathLayers}/${argument1}`;
          }
          else {
            arguments[0] = `${pathOrganization}${argument1}`;
          }
        }
        return originalRequire.apply(this, arguments);
      };
    }
    else {
      Module.prototype.require = function() {
        const argument1 = arguments[0];
        if(argument1.startsWith('z-') && !argument1.startsWith('z-b')) {
          arguments[0] = `${pathLayers}/${argument1}`;
        }
        return originalRequire.apply(this, arguments);
      };
    }
    global.tryRequire = function(path) {
      let module;
      try {
        module = require(path);
      }
      catch(err) {
        if('MODULE_NOT_FOUND' !== err.code) {
          throw err;
        }
      }
      return module;
    };
    global.abstractorRegisterApiAndStacks = AbsModule.abstractorRegisterApiAndStacks;
    global.abstractorIsApiAndStacksRegistered = AbsModule.abstractorIsApiAndStacksRegistered;
    return dynamicConfig;
  }
}

module.exports = AbsModule;
